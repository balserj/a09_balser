-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 06, 2020 at 11:03 PM
-- Server version: 8.0.21
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs3140database`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `cID` int UNSIGNED NOT NULL,
  `cpID` int UNSIGNED NOT NULL,
  `ccomment` varchar(255) NOT NULL,
  `cauthor` varchar(30) NOT NULL,
  `cauthemail` varchar(30) NOT NULL,
  `cdateposted` datetime NOT NULL,
  `cusername` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`cID`, `cpID`, `ccomment`, `cauthor`, `cauthemail`, `cdateposted`, `cusername`) VALUES
(1, 2, 'This picture is cool', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:27:50', 'balserj'),
(2, 3, 'This picture is awesome', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:28:40', 'balserj'),
(3, 3, 'This picture is bad', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:29:36', 'balserj'),
(4, 4, 'This picture is interesting', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:30:22', 'balserj'),
(5, 4, 'This picture is boring', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:31:50', 'balserj'),
(6, 4, 'This picture is good', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:32:50', 'balserj'),
(7, 4, 'This picture is clear', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:33:50', 'balserj'),
(8, 4, 'This picture is not clear', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:34:50', 'balserj'),
(9, 5, 'This picture is symbolic', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:35:50', 'balserj'),
(10, 5, 'This picture is not good', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:36:50', 'balserj'),
(11, 5, 'Very nice', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:37:50', 'balserj'),
(12, 6, 'Weird photo', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:38:50', 'balserj'),
(13, 6, 'Great photo', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:39:50', 'balserj'),
(14, 6, 'Interesting photo', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:40:50', 'balserj'),
(15, 6, 'Unnecessary photo', 'Justin Balser', 'jbalser@insight.rr.com', '2020-12-06 17:41:50', 'balserj');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `pID` int UNSIGNED NOT NULL,
  `pheading` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `psubheading` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pkeywords` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pcontent` longtext NOT NULL,
  `pallowcomment` char(1) NOT NULL DEFAULT '1',
  `pyear` varchar(4) NOT NULL DEFAULT '2009',
  `pmonth` varchar(2) NOT NULL DEFAULT '01',
  `pauthor` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Default Name',
  `pdateposted` datetime NOT NULL,
  `pusername` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`pID`, `pheading`, `psubheading`, `pkeywords`, `pcontent`, `pallowcomment`, `pyear`, `pmonth`, `pauthor`, `pdateposted`, `pusername`) VALUES
(1, 'First Blog', 'My first blog', 'first, one,\r\nun', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Feugiat in fermentum posuere urna. Aliquam ultrices sagittis orci a. Ante metus dictum at tempor. Nisl pretium fusce id velit ut tortor. Tellus integer feugiat scelerisque varius morbi. Ipsum consequat nisl vel pretium lectus quam id leo. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Erat pellentesque adipiscing commodo elit. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus. Euismod lacinia at quis risus sed vulputate.\r\n\r\nSuspendisse in est ante in nibh mauris cursus. Sed id semper risus in hendrerit gravida rutrum quisque. Convallis aenean et tortor at risus viverra adipiscing. Arcu non odio euismod lacinia. Vitae aliquet nec ullamcorper sit amet. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Risus feugiat in ante metus dictum. Eget felis eget nunc lobortis mattis aliquam faucibus purus. At erat pellentesque adipiscing commodo elit at. Mollis aliquam ut porttitor leo a diam sollicitudin tempor id. Ac tortor dignissim convallis aenean. Donec enim diam vulputate ut pharetra.\r\n\r\nEu volutpat odio facilisis mauris sit amet massa. Fusce id velit ut tortor pretium viverra suspendisse. Sodales ut etiam sit amet nisl purus. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Neque sodales ut etiam sit amet. Leo vel fringilla est ullamcorper. Vitae sapien pellentesque habitant morbi tristique senectus et. Tellus mauris a diam maecenas. Suspendisse in est ante in nibh. Nullam non nisi est sit amet facilisis. Quis imperdiet massa tincidunt nunc. Ullamcorper morbi tincidunt ornare massa eget. Nunc mi ipsum faucibus vitae. Eu non diam phasellus vestibulum lorem sed. Integer enim neque volutpat ac tincidunt vitae semper quis.</p>', '0', '2020', '12', 'Justin Balser', '2020-12-06 15:55:46', 'admin'),
(2, 'Second Blog', 'My second blog', 'second, dos', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Feugiat in fermentum posuere urna. Aliquam ultrices sagittis orci a. Ante metus dictum at tempor. Nisl pretium fusce id velit ut tortor. Tellus integer feugiat scelerisque varius morbi. Ipsum consequat nisl vel pretium lectus quam id leo. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Erat pellentesque adipiscing commodo elit. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus. Euismod lacinia at quis risus sed vulputate.\r\n\r\nSuspendisse in est ante in nibh mauris cursus. Sed id semper risus in hendrerit gravida rutrum quisque. Convallis aenean et tortor at risus viverra adipiscing. Arcu non odio euismod lacinia. Vitae aliquet nec ullamcorper sit amet. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Risus feugiat in ante metus dictum. Eget felis eget nunc lobortis mattis aliquam faucibus purus. At erat pellentesque adipiscing commodo elit at. Mollis aliquam ut porttitor leo a diam sollicitudin tempor id. Ac tortor dignissim convallis aenean. Donec enim diam vulputate ut pharetra.\r\n\r\nEu volutpat odio facilisis mauris sit amet massa. Fusce id velit ut tortor pretium viverra suspendisse. Sodales ut etiam sit amet nisl purus. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Neque sodales ut etiam sit amet. Leo vel fringilla est ullamcorper. Vitae sapien pellentesque habitant morbi tristique senectus et. Tellus mauris a diam maecenas. Suspendisse in est ante in nibh. Nullam non nisi est sit amet facilisis. Quis imperdiet massa tincidunt nunc. Ullamcorper morbi tincidunt ornare massa eget. Nunc mi ipsum faucibus vitae. Eu non diam phasellus vestibulum lorem sed. Integer enim neque volutpat ac tincidunt vitae semper quis.</p>', '1', '2020', '12', 'Justin Balser', '2020-12-06 16:01:53', 'admin'),
(3, 'Third Blog', 'My third blog', 'tres, third', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Feugiat in fermentum posuere urna. Aliquam ultrices sagittis orci a. Ante metus dictum at tempor. Nisl pretium fusce id velit ut tortor. Tellus integer feugiat scelerisque varius morbi. Ipsum consequat nisl vel pretium lectus quam id leo. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Erat pellentesque adipiscing commodo elit. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus. Euismod lacinia at quis risus sed vulputate.\r\n\r\nSuspendisse in est ante in nibh mauris cursus. Sed id semper risus in hendrerit gravida rutrum quisque. Convallis aenean et tortor at risus viverra adipiscing. Arcu non odio euismod lacinia. Vitae aliquet nec ullamcorper sit amet. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Risus feugiat in ante metus dictum. Eget felis eget nunc lobortis mattis aliquam faucibus purus. At erat pellentesque adipiscing commodo elit at. Mollis aliquam ut porttitor leo a diam sollicitudin tempor id. Ac tortor dignissim convallis aenean. Donec enim diam vulputate ut pharetra.\r\n\r\nEu volutpat odio facilisis mauris sit amet massa. Fusce id velit ut tortor pretium viverra suspendisse. Sodales ut etiam sit amet nisl purus. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Neque sodales ut etiam sit amet. Leo vel fringilla est ullamcorper. Vitae sapien pellentesque habitant morbi tristique senectus et. Tellus mauris a diam maecenas. Suspendisse in est ante in nibh. Nullam non nisi est sit amet facilisis. Quis imperdiet massa tincidunt nunc. Ullamcorper morbi tincidunt ornare massa eget. Nunc mi ipsum faucibus vitae. Eu non diam phasellus vestibulum lorem sed. Integer enim neque volutpat ac tincidunt vitae semper quis.</p>', '2', '2020', '12', 'Justin Balser', '2020-12-06 16:09:40', 'admin'),
(4, 'Fourth Blog', 'My fourth blog', 'fourth, cuatro', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Feugiat in fermentum posuere urna. Aliquam ultrices sagittis orci a. Ante metus dictum at tempor. Nisl pretium fusce id velit ut tortor. Tellus integer feugiat scelerisque varius morbi. Ipsum consequat nisl vel pretium lectus quam id leo. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Erat pellentesque adipiscing commodo elit. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus. Euismod lacinia at quis risus sed vulputate.\r\n\r\nSuspendisse in est ante in nibh mauris cursus. Sed id semper risus in hendrerit gravida rutrum quisque. Convallis aenean et tortor at risus viverra adipiscing. Arcu non odio euismod lacinia. Vitae aliquet nec ullamcorper sit amet. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Risus feugiat in ante metus dictum. Eget felis eget nunc lobortis mattis aliquam faucibus purus. At erat pellentesque adipiscing commodo elit at. Mollis aliquam ut porttitor leo a diam sollicitudin tempor id. Ac tortor dignissim convallis aenean. Donec enim diam vulputate ut pharetra.\r\n\r\nEu volutpat odio facilisis mauris sit amet massa. Fusce id velit ut tortor pretium viverra suspendisse. Sodales ut etiam sit amet nisl purus. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Neque sodales ut etiam sit amet. Leo vel fringilla est ullamcorper. Vitae sapien pellentesque habitant morbi tristique senectus et. Tellus mauris a diam maecenas. Suspendisse in est ante in nibh. Nullam non nisi est sit amet facilisis. Quis imperdiet massa tincidunt nunc. Ullamcorper morbi tincidunt ornare massa eget. Nunc mi ipsum faucibus vitae. Eu non diam phasellus vestibulum lorem sed. Integer enim neque volutpat ac tincidunt vitae semper quis.</p>', '5', '2020', '12', 'Justin Balser', '2020-12-06 16:10:15', 'admin'),
(5, 'Fifth Blog', 'My fifth blog', 'fifth, cinco', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Feugiat in fermentum posuere urna. Aliquam ultrices sagittis orci a. Ante metus dictum at tempor. Nisl pretium fusce id velit ut tortor. Tellus integer feugiat scelerisque varius morbi. Ipsum consequat nisl vel pretium lectus quam id leo. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Erat pellentesque adipiscing commodo elit. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus. Euismod lacinia at quis risus sed vulputate.\r\n\r\nSuspendisse in est ante in nibh mauris cursus. Sed id semper risus in hendrerit gravida rutrum quisque. Convallis aenean et tortor at risus viverra adipiscing. Arcu non odio euismod lacinia. Vitae aliquet nec ullamcorper sit amet. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Risus feugiat in ante metus dictum. Eget felis eget nunc lobortis mattis aliquam faucibus purus. At erat pellentesque adipiscing commodo elit at. Mollis aliquam ut porttitor leo a diam sollicitudin tempor id. Ac tortor dignissim convallis aenean. Donec enim diam vulputate ut pharetra.\r\n\r\nEu volutpat odio facilisis mauris sit amet massa. Fusce id velit ut tortor pretium viverra suspendisse. Sodales ut etiam sit amet nisl purus. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Neque sodales ut etiam sit amet. Leo vel fringilla est ullamcorper. Vitae sapien pellentesque habitant morbi tristique senectus et. Tellus mauris a diam maecenas. Suspendisse in est ante in nibh. Nullam non nisi est sit amet facilisis. Quis imperdiet massa tincidunt nunc. Ullamcorper morbi tincidunt ornare massa eget. Nunc mi ipsum faucibus vitae. Eu non diam phasellus vestibulum lorem sed. Integer enim neque volutpat ac tincidunt vitae semper quis.</p>', '3', '2020', '12', 'Justin Balser', '2020-12-06 16:11:03', 'admin'),
(6, 'Sixth Blog', 'My sixth blog', 'sixth, seis', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Feugiat in fermentum posuere urna. Aliquam ultrices sagittis orci a. Ante metus dictum at tempor. Nisl pretium fusce id velit ut tortor. Tellus integer feugiat scelerisque varius morbi. Ipsum consequat nisl vel pretium lectus quam id leo. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Erat pellentesque adipiscing commodo elit. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus. Euismod lacinia at quis risus sed vulputate.\r\n\r\nSuspendisse in est ante in nibh mauris cursus. Sed id semper risus in hendrerit gravida rutrum quisque. Convallis aenean et tortor at risus viverra adipiscing. Arcu non odio euismod lacinia. Vitae aliquet nec ullamcorper sit amet. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Risus feugiat in ante metus dictum. Eget felis eget nunc lobortis mattis aliquam faucibus purus. At erat pellentesque adipiscing commodo elit at. Mollis aliquam ut porttitor leo a diam sollicitudin tempor id. Ac tortor dignissim convallis aenean. Donec enim diam vulputate ut pharetra.\r\n\r\nEu volutpat odio facilisis mauris sit amet massa. Fusce id velit ut tortor pretium viverra suspendisse. Sodales ut etiam sit amet nisl purus. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Neque sodales ut etiam sit amet. Leo vel fringilla est ullamcorper. Vitae sapien pellentesque habitant morbi tristique senectus et. Tellus mauris a diam maecenas. Suspendisse in est ante in nibh. Nullam non nisi est sit amet facilisis. Quis imperdiet massa tincidunt nunc. Ullamcorper morbi tincidunt ornare massa eget. Nunc mi ipsum faucibus vitae. Eu non diam phasellus vestibulum lorem sed. Integer enim neque volutpat ac tincidunt vitae semper quis.</p>', '4', '2020', '12', 'Justin Balser', '2020-12-06 16:12:38', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`cID`);
ALTER TABLE `comments` ADD FULLTEXT KEY `ccomment` (`ccomment`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`pID`);
ALTER TABLE `posts` ADD FULLTEXT KEY `pheading` (`pheading`,`pkeywords`);
ALTER TABLE `posts` ADD FULLTEXT KEY `pcontent` (`pcontent`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `cID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `pID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
