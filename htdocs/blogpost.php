<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Assignment09 -- Balser, Justin</title>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="styles.css">
<?php require_once(INC_HEADER);?>
<body class="w3-light-grey">
<!-- Blog entry -->
<div class="w3-card-4 w3-margin w3-white">
 <img src="images/woods.jpg" alt="Nature" style="width:100%">
 <div class="w3-container">
 <h3><b>
<?php
//TODO: display “TITLE HEADING” here
 ?>
 </b></h3>
 <h5>
<?php //TODO: display “My first blog entry,
<span class="w3-opacity">January 27, 2018” in this span ?></span>
 </h5>
 </div>
 <div class="w3-container">
<?php
// TODO: display only the first paragraph of this post here… with “…” added at the
// end of the paragraph.
<p> First blog entry. This is fun. Mauris neque quam, fermentum ut nisl vitae,
convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor
magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam
corper. Praesent tincidunt se tellus ut rutrum. Sed vitae justo condimentum, porta
lectus vitae, ultricies congue gravida diam non fringilla.</p>
 ?>
 <div class="w3-row">
 <div class="w3-col m8 s12">
 <p><button class="w3-button w3-padding-large w3-white w3-border">
<b>READ MORE</b></button></p>
 </div>
 <div class="w3-col m4 w3-hide-small">
 <p><span class="w3-padding-large w3-right"><b>Comments</b>
 <span class="w3-tag"> <?php // TODO:show # of comments ?> </span></span></p>
 </div>
 </div>
</div> <!-- w3-container -->
</div> <!-- w3-card-4 w3-margin w3-white -->
</body>
<?php require_once(INC_FOOTER);?>
</html>
